package cybot.util;

public class Contstants {
    public static final String SMART = "/cy_smart";
    public static final String SMART2= "КЯ мудрость";
    public static final String CURRENCY = "КЯ курс";
    public static final String CURRENCY2 = "/cy_currency";
    public static final String WEATHER = "КЯ погода";
    public static final String WEATHER2 = "/cy_weather";
    public static final String START = "/start";
    public static final String HELP = "/help";
    public static final String GAME = "/cy_game";
    public static final String GAME2 = "КЯ игра";
    public static final String GAME3 = "/cy_gametic";
    public static final String GAME4 = "КЯ кресты";
    public static final String HORO = "/cy_horo";
    public static final String HORO2 = "КЯ гороскоп";
    public static final String REMIND = "/cy_remind";
    public static final String REMIND2 = "Напомни";
}
