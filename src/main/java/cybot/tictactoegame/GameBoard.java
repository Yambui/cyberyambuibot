package cybot.tictactoegame;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class GameBoard {

    private Values[] board;
    private boolean isGameEnded;
    private String firstPlayer;
    private Values winner;

    GameBoard(){
        this.board = new Values[] {Values.NO, Values.NO, Values.NO, Values.NO, Values.NO, Values.NO, Values.NO, Values.NO,Values.NO};
        this.isGameEnded = false;
        this.winner = null;
        int rnd = ThreadLocalRandom.current().nextInt(1,  3);
        if (rnd == 1) {
            this.firstPlayer = "CPU";
        } else {
            this.firstPlayer = "HUM";
        }
    }

    public String getFirstPlayer() {
        return firstPlayer;
    }

    public ReplyKeyboardMarkup toReplyKeyboard(){
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        for(int i = 0; i < 9; i++){
            if (this.board[i]==Values.NO){
                row.add(String.valueOf(i+1));
            } else {
                row.add(this.board[i].getValue());
            }
            if ((i==2)||(i==5)||(i==8)){
                keyboard.add((KeyboardRow) row.clone());
                row.clear();
            }
        }
        keyboardMarkup.setKeyboard(keyboard);
        keyboardMarkup.setOneTimeKeyboard(true);
        return keyboardMarkup;
    }

    public String toString () {
        StringBuilder result = new StringBuilder("Игровая доска \n\r");
        for(int i = 0; i < 9; i++) {
            result.append(this.board[i].getValue()+" |");
            if ((i == 2) || (i == 5) || (i == 8)) {
                result.deleteCharAt(result.length()-1);
                result.append("\n\r");
            }
        }
        result.append("_____________");
        return result.toString();
    }

    public boolean isNotOccupied (int index){
        if (this.board[index] == Values.NO){
            return true;
        }
        return false;
    }

    private void setWinnerEndgame(Values winner){
        this.winner = winner;
        this.isGameEnded = true;
    }

    public void endGameCheck (){

        if (((board[0] == board[1]) && (board[0] == board[2]))
                || ((board[0] == board[3]) && (board[0] == board[6]))) {
            if (board[0] != Values.NO) {
                setWinnerEndgame(board[0]);
                return;
            }
        }
        if (((board[4] == board[3]) && (board[4] == board[5]))
                || ((board[4] == board[1]) && (board[4] == board[7]))
                || ((board[4] == board[0]) && (board[4] == board[8]))
                || ((board[4] == board[2]) && (board[4] == board[6]))) {
            if (board[4] != Values.NO) {
                setWinnerEndgame(board[4]);
                return;
            }
        }
        if (((board[8] == board[2]) && (board[8] == board[5]))
                || ((board[8] == board[6]) && (board[8] == board[7]))) {
            if (board[8] != Values.NO) {
                setWinnerEndgame(board[8]);
                return;
            }
        }
        if (!Arrays.asList(board).contains(Values.NO)) {
            setWinnerEndgame(Values.NO);
        }

    }

    public boolean setValue(int position, Values value){
        if ((isGameEnded)||(!isNotOccupied(position))) {
            return false;
        }
        board[position] = value;
        return true;
    }

    public Values[] getBoard() {
        return board;
    }

    public boolean isGameEnded() {
        return isGameEnded;
    }

    public Values getWinner() {
        return winner;
    }


}
