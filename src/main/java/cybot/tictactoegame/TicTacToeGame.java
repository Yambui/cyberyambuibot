package cybot.tictactoegame;

import org.glassfish.grizzly.utils.Pair;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;


public class TicTacToeGame {

    GameBoard gameBoard;

    public SendMessage ticGameStart(SendMessage message) {
        gameBoard = new GameBoard();
        if (gameBoard.getFirstPlayer().equals("CPU")) {
            BotAI botAI = new BotAI();
            gameBoard.setValue(botAI.doMove(gameBoard),Values.ZERO);
        }
        message.setText("Да начнется битва ⭕❌ \n\r" + gameBoard.toString());
        message.setReplyMarkup(gameBoard.toReplyKeyboard());
        return message;
    }

    public Pair<Boolean, SendMessage> humanInput(SendMessage message) {
        int human = -1;
        try {
            human = Integer.parseInt(message.getText())-1;
        } catch (Exception e){
           return setUpInvalid(message);
        }
        if ((human < 0)||(human >8)||!gameBoard.setValue(human,Values.CROSS)) {
            return setUpInvalid(message);
        }
        gameBoard.endGameCheck();
        if (gameBoard.isGameEnded()) {
            return setUpWinMessage(message);
        }
        BotAI botAI = new BotAI();
        gameBoard.setValue(botAI.doMove(gameBoard),Values.ZERO);
        gameBoard.endGameCheck();
        if (gameBoard.isGameEnded()) {
            return setUpWinMessage(message);
        }
        message.setText(gameBoard.toString());
        message.setReplyMarkup(gameBoard.toReplyKeyboard());
        return new Pair<>(false,message);
    }

    private Pair<Boolean, SendMessage> setUpWinMessage(SendMessage message){
        StringBuilder response = new StringBuilder();
        response.append("Игра оконченна \n\r");
        if (gameBoard.getWinner().equals(Values.NO)) {
            response.append("\uD83D\uDC7B Ничья! \uD83D\uDC7B\n\r");
        }
        if (gameBoard.getWinner().equals(Values.CROSS)) {
            response.append("Ты победил, человечишка, но только в этот раз. \uD83D\uDC8B\uD83C\uDF77\uD83C\uDF78\n\r");
        }
        if (gameBoard.getWinner().equals(Values.ZERO)) {
            response.append("Ты КИБЕРУНИЖЕН! \uD83D\uDCA9♿\uD83D\uDCA9\n\r");
        }
        response.append(gameBoard.toString());
        message.setText(response.toString());
        message.setReplyMarkup(new ReplyKeyboardRemove());
        return new Pair<>(true,message);
    }

    private Pair<Boolean, SendMessage> setUpInvalid(SendMessage message) {
        String resp = "Импут инвалид, как и ты. ♿\uD83D\uDE21\uD83D\uDE21\uD83D\uDE21 \n\r" + gameBoard.toString();
        message.setText(resp);
        message.setReplyMarkup(gameBoard.toReplyKeyboard());
        return new Pair<>(false,message);
    }
}
