package cybot.tictactoegame;

public enum Values {
    CROSS("❌"),
    ZERO("⭕"),
    NO("➖");

    private final String value;

    String getValue() {
        return value;
    }

    Values(String value){
        this.value = value ;
    }

}
