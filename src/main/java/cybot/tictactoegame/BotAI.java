package cybot.tictactoegame;


import java.util.ArrayList;
import java.util.List;


public class BotAI {

    Values aiPlayer = Values.ZERO;
    Values huPlayer = Values.CROSS;

    public int doMove(GameBoard gboard) {
        int pos = miniMax(gboard.getBoard(), aiPlayer)[1];
        return pos;
    }

    private boolean isPlayerWinning(Values[] board, Values player) {
        if (            (board[0] == player && board[1] == player && board[2] == player) ||
                        (board[3] == player && board[4] == player && board[5] == player) ||
                        (board[6] == player && board[7] == player && board[8] == player) ||
                        (board[0] == player && board[3] == player && board[6] == player) ||
                        (board[1] == player && board[4] == player && board[7] == player) ||
                        (board[2] == player && board[5] == player && board[8] == player) ||
                        (board[0] == player && board[4] == player && board[8] == player) ||
                        (board[2] == player && board[4] == player && board[6] == player)
                ) {
            return true;
        } else {
            return false;
        }
    }

    private List<Integer> emptyIndexes(Values[] board) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            if (board[i] == Values.NO) {
                result.add(i);
            }
        }
        return result;
    }

    private int[] miniMax(Values[] board, Values player) {

        List<Integer> availableSpots = emptyIndexes(board);
        int currentScore;
        int bestMov = -1;
        int bestScore;

        if (player == Values.ZERO){
           bestScore = Integer.MIN_VALUE;
        } else {
           bestScore = Integer.MAX_VALUE;
        }

        if (availableSpots.isEmpty()) {
            if (isPlayerWinning(board, huPlayer)) {
                bestScore = -10;
            } else if (isPlayerWinning(board, aiPlayer)) {
                bestScore = 10;
            } else {
                bestScore = 0;
            }
        } else {
            for (int i : availableSpots) {
                board[i] = player;
                if (player == aiPlayer) {
                    currentScore = miniMax(board, huPlayer)[0];
                    if (currentScore > bestScore) {
                        bestScore = currentScore;
                        bestMov = i;
                    }
                } else {
                    currentScore = miniMax(board, aiPlayer)[0];
                    if (currentScore < bestScore) {
                        bestScore = currentScore;
                        bestMov = i;
                    }
                }
                board[i] = Values.NO;
            }
        }
        return new int[]{bestScore, bestMov};
    }

}
