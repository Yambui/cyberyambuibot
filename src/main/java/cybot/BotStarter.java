package cybot;

import cybot.horoscope.HoroscopeWorker;
import cybot.kafka.ElasticConsumer;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BotStarter {
    public static void main(String[] args) {
        System.out.println("Starting BOT!!");
        // Initialize Api Context
        ApiContextInitializer.init();

        // Instantiate Telegram Bots API
        TelegramBotsApi botsApi = new TelegramBotsApi();

        // Register our bot
        CyberYambuiBot botInstance = new CyberYambuiBot();
        try {
            botsApi.registerBot(botInstance);
            System.out.println("Bot registered!");
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        // Start elastic Consumer
        ElasticConsumer consumer = new ElasticConsumer();
        HoroscopeWorker horoscopeWorker = new HoroscopeWorker();
        ScheduledExecutorService elasticExecutor = Executors.newSingleThreadScheduledExecutor();
        elasticExecutor.scheduleAtFixedRate(consumer,2,2, TimeUnit.MINUTES);
        ScheduledExecutorService horoscopeExecutor = Executors.newSingleThreadScheduledExecutor();
        horoscopeExecutor.scheduleAtFixedRate(horoscopeWorker,0,20, TimeUnit.HOURS);
        ScheduledExecutorService reminderExecutor = Executors.newSingleThreadScheduledExecutor();
        reminderExecutor.scheduleAtFixedRate(botInstance,5,5, TimeUnit.MINUTES);
    }
}
