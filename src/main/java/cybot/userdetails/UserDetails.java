package cybot.userdetails;

import cybot.RPSGame;
import cybot.horoscope.HoroscopeResponder;
import cybot.tictactoegame.TicTacToeGame;

public class UserDetails {

    private Boolean rpsGameStarted = false;
    private Boolean ticGameStarted = false;
    private Boolean horoscopeStarted = false;
    private Long chatId;
    private final TicTacToeGame ticTacToeGame = new TicTacToeGame();
    private final HoroscopeResponder horoscopeResponder = new HoroscopeResponder();
    private final RPSGame rpsGame = new RPSGame();

    public UserDetails(Long chatId) {
        this.chatId = chatId;
    }

    public TicTacToeGame getTicTacToeGame() {
        return ticTacToeGame;
    }

    public HoroscopeResponder getHoroscopeResponder() {
        return horoscopeResponder;
    }

    public RPSGame getRpsGame() {
        return rpsGame;
    }

    public Boolean getRpsGameStarted() {
        return rpsGameStarted;
    }

    public void setRpsGameStarted(Boolean rpsGameStarted) {
        this.rpsGameStarted = rpsGameStarted;
    }

    public Boolean getTicGameStarted() {
        return ticGameStarted;
    }

    public void setTicGameStarted(Boolean ticGameStarted) {
        this.ticGameStarted = ticGameStarted;
    }

    public Boolean getHoroscopeStarted() {
        return horoscopeStarted;
    }

    public void setHoroscopeStarted(Boolean horoscopeStarted) {
        this.horoscopeStarted = horoscopeStarted;
    }

    public Long getChatId() {
        return chatId;
    }
}
