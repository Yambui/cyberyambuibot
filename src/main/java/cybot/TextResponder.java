package cybot;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jramoyo.io.IndexedFileReader;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class TextResponder {

    static ObjectMapper mapper = new ObjectMapper();
    final PropsLoader propsLoader = PropsLoader.getInstance();

    public String getRandomIdiom() {
        File file = null;
        int randomNum = ThreadLocalRandom.current().nextInt(1, propsLoader.getUkrIdiomsNumberOfLines() + 1);
        try {
            file = File.createTempFile("tempfile", ".tmp");
            InputStream inputStream1 = getClass().getClassLoader().getResourceAsStream("ukridioms.txt");
            FileUtils.copyInputStreamToFile(inputStream1, file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String idiom = "Error";
        try (IndexedFileReader reader = new IndexedFileReader(file)) {
            idiom = reader.readLines(randomNum, randomNum).get(randomNum);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return idiom;
    }

    public String getCurrencyRate(){
        List<HashMap> currencies = null;
        List<String> response = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String nbuRequest = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date="+(formatter.format(date))+"&json";
        int numberOfTries = 0;

        do {
            try {
                currencies = mapper.readValue(jsonGetRequest(nbuRequest), new TypeReference<List<HashMap>>() {});
                break;
            } catch (Exception e) {
                numberOfTries++;
            }
        } while(numberOfTries < 3);

        if (numberOfTries >= 3) {
            return "сорре, сервер умер";
        }

        response.add("Повелитель, курс иностранной валюты по НБУ на сегодня("+currencies.get(1).get("exchangedate") +"):");
        for (HashMap map: currencies){
            if (map.get("cc").equals("USD")) {
                response.add("Доларес благородный: "+ map.get("rate"));
            }
            if (map.get("cc").equals("EUR")) {
                response.add("ЕВРО благочестивый: "+ map.get("rate"));
            }
            if (map.get("cc").equals("RUB")) {
                response.add("Рубель вонючий: "+ map.get("rate"));
            }
            if (map.get("cc").equals("GBP")) {
                response.add("Фунт заморскай: "+ map.get("rate"));
            }
        }
        return messageTextBuilder(response);
    }

    public String getWeatherForecast(){

        String weatherurl = "http://api.openweathermap.org/data/2.5/weather?id=709932&lang=ru&APPID="+propsLoader.getWeathertoken();
        String kk = jsonGetRequest(weatherurl);
        HashMap<String,Object> weather = null;
        try {
            weather = mapper.readValue(kk, new TypeReference<HashMap>(){});
        } catch (Exception e) {
            return "Сорре, сервер умир";
        }
        List<String> response = new ArrayList<>();
        response.add("Повелитель, погода в Днепродыме: ");
        response.add("Температура:  "+(((Double)((HashMap)weather.get("main")).get("temp")) - 273.15)+" Цельсия");
        response.add("Давление:  "+((HashMap)weather.get("main")).get("pressure")+ " МПа");
        response.add("Влажность:  "+((HashMap)weather.get("main")).get("humidity")+ " %");
        response.add("Облачность:  "+(((HashMap)((List)weather.get("weather")).get(0)).get("description")));
        response.add("Ветер:  " + ((HashMap)weather.get("wind")).get("speed")+ " хз чего");
        return messageTextBuilder(response);
    }

    public static String jsonGetRequest(String urlQueryString) {
        String json = null;
        try {
            URL url = new URL(urlQueryString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            json = new Scanner(inStream, "UTF-8").useDelimiter("\\Z").next(); // input stream to string
        } catch (IOException ex) {
            System.out.println("Could not get response from server " + urlQueryString);
        }
        if (json == null) {
            return null;
        } else {
            json = json.replaceAll("\r", "").replaceAll("\n", "").replaceAll(" ", "");
            return json;
        }
    }

    private String messageTextBuilder(List<String> args){
        StringBuilder builder = new StringBuilder();
        for (String s : args){
            builder.append(s);
            builder.append("\r\n");
        }
        return builder.toString();
    }

    public String getStartMessage (){
        String string = "Вас приветствует Кибер Ямбуй Бот \n " +
                "Доступные команды: \"КЯ курс\" \"КЯ мудрость\" \"КЯ погода\" \"КЯ игра\" \"КЯ кресты\" \"КЯ гороскоп\" \"Напомни МИН напоминание\"";
        return string;
    }

}
