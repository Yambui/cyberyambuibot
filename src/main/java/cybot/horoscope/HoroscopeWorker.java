package cybot.horoscope;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public class HoroscopeWorker implements Runnable {

    private static final String EROTIC_HORO = "eroticHoro";
    private static final String EROTIC_HORO_URL = "http://ignio.com/r/export/utf/xml/daily/ero.xml";
    private static final String DAILY_HORO = "dailyHoro";
    private static final String DAILY_HORO_URL = "http://ignio.com/r/export/utf/xml/daily/com.xml";
    private static final String ANTI_HORO = "antiHoro";
    private static final String ANTI_HORO_URL = "http://ignio.com/r/export/utf/xml/daily/anti.xml";

    private static HoroscopeMap eroticHoroscope = new HoroscopeMap();
    private static HoroscopeMap dailyHoroscope = new HoroscopeMap();
    private static HoroscopeMap antiHoroscope = new HoroscopeMap();

    public static HoroscopeMap getEroticHoroscope() {
        return eroticHoroscope;
    }

    public static HoroscopeMap getDailyHoroscope() {
        return dailyHoroscope;
    }

    public static HoroscopeMap getAntiHoroscope() {
        return antiHoroscope;
    }

    private void populateTempFileFromHoroUrl(File file, String url) {
        try {
            ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(url).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            FileChannel fileChannel = fileOutputStream.getChannel();
            fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        } catch (Exception ex) {
            System.out.println("Unable to populate " + file.getName() + " " + ex.toString());
        }
    }

    @Override
    public void run() {

        try {
            System.out.println("Starting horoscope population");
            File eroticHoroscop = File.createTempFile(EROTIC_HORO, ".xml");
            populateTempFileFromHoroUrl(eroticHoroscop, EROTIC_HORO_URL);
            eroticHoroscope.populateHoroMap(eroticHoroscop);
            File dailyHoroscop = File.createTempFile(DAILY_HORO, ".xml");
            populateTempFileFromHoroUrl(dailyHoroscop, DAILY_HORO_URL);
            dailyHoroscope.populateHoroMap(dailyHoroscop);
            File antiHoroscop = File.createTempFile(ANTI_HORO, ".xml");
            populateTempFileFromHoroUrl(antiHoroscop, ANTI_HORO_URL);
            antiHoroscope.populateHoroMap(antiHoroscop);
            System.out.println("Horoscopes Populated");
        } catch (Exception e) {
            System.out.println("Could not create TMP files " + e.toString());
        }
    }
}
