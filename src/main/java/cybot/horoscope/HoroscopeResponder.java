package cybot.horoscope;

import org.glassfish.grizzly.utils.Pair;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.*;
import java.util.stream.Collectors;

public class HoroscopeResponder {

    private static final String USUAL = "Ежедневный гороскоп \uD83D\uDED0 \uD83D\uDD2D \uD83D\uDD2E";
    private static final String EROTIC = "Эротический гороскоп \uD83D\uDC8B \uD83D\uDC98 \uD83D\uDC94";
    private static final String ANTI = "Анти гороскоп \uD83C\uDF1C ✨ \uD83C\uDF1A";

    private static final Map<String, String> buttonsToHoroscopes;

    static {
        buttonsToHoroscopes = new HashMap<>();
        buttonsToHoroscopes.put("Овен ♈", "aries");
        buttonsToHoroscopes.put("Телец ♉", "taurus");
        buttonsToHoroscopes.put("Близнецы ♊", "gemini");
        buttonsToHoroscopes.put("Рак ♋", "cancer");
        buttonsToHoroscopes.put("Лев ♌", "leo");
        buttonsToHoroscopes.put("Дева ♍", "virgo");
        buttonsToHoroscopes.put("Весы ♎", "libra");
        buttonsToHoroscopes.put("Скорпион ♏", "scorpio");
        buttonsToHoroscopes.put("Стрелец ♐", "sagittarius");
        buttonsToHoroscopes.put("Козерог ♑", "capricorn");
        buttonsToHoroscopes.put("Водолей ♒", "aquarius");
        buttonsToHoroscopes.put("Рыбы ♓", "pisces");
    }

    private HoroscopeMap choosenType = null;

    public SendMessage horoscopeStart(SendMessage message) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add(USUAL);
        KeyboardRow row1 = new KeyboardRow();
        row1.add(EROTIC);
        KeyboardRow row2 = new KeyboardRow();
        row2.add(ANTI);
        keyboard.add(row);
        keyboard.add(row1);
        keyboard.add(row2);
        keyboardMarkup.setKeyboard(keyboard);
        keyboardMarkup.setOneTimeKeyboard(true);
        message.setReplyMarkup(keyboardMarkup);
        message.setText("Выбери свою судьбу: \uD83C\uDFB0");
        return message;
    }

    public Pair<Boolean, SendMessage> processResponseFromUser(SendMessage message) {
        String messageText = message.getText();
        if (Arrays.asList(USUAL, ANTI, EROTIC).contains(messageText)) {
            switch (messageText) {
                case USUAL: {
                    choosenType = HoroscopeWorker.getDailyHoroscope();
                }
                break;
                case ANTI: {
                    choosenType = HoroscopeWorker.getAntiHoroscope();
                }
                break;
                case EROTIC: {
                    choosenType = HoroscopeWorker.getEroticHoroscope();
                }
                break;
            }
                message.setText("Выбери знак зодиака: ☮");
                message.setReplyMarkup(constructKeybord());
                return new Pair<Boolean, SendMessage>(false, message);
            } else{
                return replyWithHoroscope(message);
            }
        }

    private Pair<Boolean, SendMessage> replyWithHoroscope(SendMessage message) {
        if (buttonsToHoroscopes.keySet().contains(message.getText())) {
            String keyForZodiac = buttonsToHoroscopes.get(message.getText());
            Horoscope horoscope = (Horoscope) choosenType.getHoroMap().get(keyForZodiac);
            if (horoscope != null) {
                message.setText(horoscope.toString());
            } else {
                message.setText("Гороскопов не завезли. \uD83E\uDD15 На сервере проблемы. \uD83D\uDE2D");
            }
            message.setReplyMarkup(new ReplyKeyboardRemove());
            return new Pair<>(true, message);
        } else {
            message.setText("Я тебе какое плохое зло сделал \uD83E\uDD16? зачем Ты так со мной \uD83D\uDE21\uD83E\uDD2C?");
            message.setReplyMarkup(new ReplyKeyboardRemove());
            return new Pair<>(true, message);
        }
    }

    private ReplyKeyboardMarkup constructKeybord() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        List<String> zodiak = buttonsToHoroscopes.keySet().stream().collect(Collectors.toList());
        KeyboardRow row = new KeyboardRow();
        row.add(zodiak.get(0));
        row.add(zodiak.get(1));
        row.add(zodiak.get(2));
        KeyboardRow row1 = new KeyboardRow();
        row1.add(zodiak.get(3));
        row1.add(zodiak.get(4));
        row1.add(zodiak.get(5));
        KeyboardRow row2 = new KeyboardRow();
        row2.add(zodiak.get(6));
        row2.add(zodiak.get(7));
        row2.add(zodiak.get(8));
        KeyboardRow row3 = new KeyboardRow();
        row3.add(zodiak.get(9));
        row3.add(zodiak.get(10));
        row3.add(zodiak.get(11));
        keyboard.add(row);
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        keyboardMarkup.setKeyboard(keyboard);
        keyboardMarkup.setOneTimeKeyboard(true);
        return keyboardMarkup;
    }
}



