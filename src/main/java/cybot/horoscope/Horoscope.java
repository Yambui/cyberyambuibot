package cybot.horoscope;

public class Horoscope {

    private String yesterday;
    private String today;
    private String tomorrow;
    private String afterTomorrow;

    public Horoscope(String yesterday, String today, String tomorrow, String afterTomorrow) {
        this.yesterday = yesterday;
        this.today = today;
        this.tomorrow = tomorrow;
        this.afterTomorrow = afterTomorrow;
    }

    public String getYesterday() {
        return yesterday;
    }

    public void setYesterday(String yesterday) {
        this.yesterday = yesterday;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getTomorrow() {
        return tomorrow;
    }

    public void setTomorrow(String tomorrow) {
        this.tomorrow = tomorrow;
    }

    public String getAfterTomorrow() {
        return afterTomorrow;
    }

    public void setAfterTomorrow(String afterTomorrow) {
        this.afterTomorrow = afterTomorrow;
    }

    @Override
    public String toString() {
        StringBuilder horoscope = new StringBuilder();
        horoscope.append("Твоя судьба:\uD83D\uDE48 \uD83D\uDE49 \uD83D\uDE4A\n")
                .append("Вчера:⬆")
                .append(yesterday)
                .append("Сегодня:➡")
                .append(today)
                .append("Завтра:↘")
                .append(tomorrow)
                .append("Послезавтра:⬇")
                .append(afterTomorrow);
        return horoscope.toString();
    }
}
