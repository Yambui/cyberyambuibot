package cybot.horoscope;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class HoroscopeMap {

    private Map<String, Horoscope> horoMap;

    public HoroscopeMap() {
        this.horoMap = new HashMap<String, Horoscope>();
        horoMap.put("aries", null);
        horoMap.put("taurus", null);
        horoMap.put("gemini", null);
        horoMap.put("cancer", null);
        horoMap.put("leo", null);
        horoMap.put("virgo", null);
        horoMap.put("libra", null);
        horoMap.put("scorpio", null);
        horoMap.put("sagittarius", null);
        horoMap.put("capricorn", null);
        horoMap.put("aquarius", null);
        horoMap.put("pisces", null);
    }

    public Map getHoroMap() {
        return horoMap;
    }

    private List<String> buildXPassList(String element) {
        List <String> expressionsForElement = new ArrayList<>();
        Arrays.asList("yesterday","today","tomorrow","tomorrow02").forEach(
                el -> expressionsForElement.add("/horo/"+ element +"/"+el)
        );
        return expressionsForElement;
    }

    public void populateHoroMap(File file) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new FileInputStream(file));
            XPath xPath = XPathFactory.newInstance().newXPath();
            for (String key : horoMap.keySet()) {
                List<String> requests = buildXPassList(key);
                List<String> responces = new ArrayList<>();
                for (String request : requests) {
                    responces.add(xPath.compile(request).evaluate(document));
                }
                Horoscope horoscope = new Horoscope(
                        responces.get(0),responces.get(1),responces.get(2),responces.get(3)
                );
                horoMap.put(key,horoscope);
            }
        } catch (Exception ex) {
            System.out.println("Unable to populate HoroMap :( "+ ex.toString());
        }
    }
}
