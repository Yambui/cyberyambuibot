package cybot.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cybot.PropsLoader;
import cybot.reminder.Reminder;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Set;

public class JedisProvider {

    private static Jedis connection = getJedis();
    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    private static Jedis createJedis() {
        System.out.println("Starting REDIS");
        Jedis jedis = new Jedis(PropsLoader.getInstance().getREDIS_HOST(), 16593);
        System.out.println("Starting REDIS - 2");
        jedis.auth(PropsLoader.getInstance().getREDIS_PASSWORD());
        System.out.println("Connected to REDIS");
        return jedis;
    }

    private static Jedis getJedis() {
        if (connection == null) {
            connection = createJedis();
        }
        return connection;
    }

    public static void addToRemindersMap (Reminder reminder) throws JsonProcessingException {
        getJedis().set(reminder.getReminderTime().toString(), objectMapper.writeValueAsString(reminder));
    }

    public static Reminder getReminder (String key) throws IOException {
        String reminder = getJedis().get(key);
        return objectMapper.readValue(reminder, Reminder.class);
    }

    public static void removeReminder (String key) {
        getJedis().del(key);
    }

    public static Set<String> getAllKeys() {
        return getJedis().keys("*");
    }
}
