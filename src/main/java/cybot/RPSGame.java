package cybot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RPSGame {
    String stone = "✊ Камень";
    String scissors = "✌ Ножницы";
    String paper = "✋ Бумага";

    public SendMessage rpsGameStart(SendMessage message) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add(stone);
        row.add(scissors);
        row.add(paper);
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        keyboardMarkup.setOneTimeKeyboard(true);
        message.setReplyMarkup(keyboardMarkup);
        message.setText("Да начнется битва ✊✌✋");
        return message;
    }

    public SendMessage doGame(SendMessage message) {
        String reply = message.getText();
        if (reply.equals(stone) || reply.equals(scissors) || reply.equals(paper)) {
            Integer randomNum = ThreadLocalRandom.current().nextInt(1,  4);
            message.setText(checkWin(reply,randomNum));
        } else {
            message.setText("ПОТРАЧЕНО, ты проиграл \uD83D\uDC80");
        }
        message.setReplyMarkup(new ReplyKeyboardRemove());
        return message;
    }

    private String checkWin (String human, Integer computer){
        String response = "";
        String pcchoice = "";
        switch (computer){
            case 1: {
                pcchoice = stone;
            }
            break;
            case 2: {
                pcchoice = scissors;
            }
            break;
            case 3: {
                pcchoice = paper;
            }
            break;
        }
        response = pcchoice + "\n\r";
        if ((human.equals(stone)&&pcchoice.equals(scissors))||(human.equals(scissors)&&pcchoice.equals(paper))
                ||(human.equals(paper)&&pcchoice.equals(stone))) {
            response = response + "Ты победил, но только в этот раз. \uD83D\uDC96";
        } else if (human.equals(pcchoice)) {
            response = response + "Ничья, Повезет в будущем. \uD83D\uDD04";
        } else {
            response = response + "ВЫ КИБЕРУНИЖЕНЫ. \uD83D\uDC68➡\uD83D\uDEBD";
        }
        return response;
    }
}
