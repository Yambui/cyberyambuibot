package cybot.reminder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;

public class Reminder {

    private Long chatId;
    private String reminderText;
    @JsonIgnore
    private LocalDateTime reminderTime;

    public Reminder(Long chatId, String reminderText, LocalDateTime reminderTime) {
        this.chatId = chatId;
        this.reminderText = reminderText;
        this.reminderTime = reminderTime;
    }

    public Reminder() {
    }

    public Long getChatId() {
        return chatId;
    }

    public String getReminderText() {
        return reminderText;
    }

    public LocalDateTime getReminderTime() {
        return reminderTime;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public void setReminderText(String reminderText) {
        this.reminderText = reminderText;
    }


    public void setReminderTime(LocalDateTime reminderTime) {
        this.reminderTime = reminderTime;
    }
}
