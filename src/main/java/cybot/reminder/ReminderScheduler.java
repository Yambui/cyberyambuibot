package cybot.reminder;

import com.fasterxml.jackson.core.JsonProcessingException;
import cybot.redis.JedisProvider;
import cybot.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static cybot.util.Contstants.REMIND;
import static cybot.util.Contstants.REMIND2;

public class ReminderScheduler {

    public static String tryToScheduleReminder(String messageText, UserDetails userDetails) {
        String reply = messageText;
        if (messageText.startsWith(REMIND) || messageText.startsWith(REMIND2)) {
            List<String> dividedInput = Arrays.asList(messageText.split("[\\s,;]+"));
            if (dividedInput.size() > 2) {
                int delay = 0;
                try {
                    delay = Math.abs(Integer.parseInt(dividedInput.get(1)));
                } catch (NumberFormatException ex) {
                    System.out.println("Could not parse time");
                }
                if (delay != 0) {
                    String reminderText = dividedInput.stream().skip(2).collect(Collectors.joining(" "));
                    Reminder reminder1 = new Reminder(
                            userDetails.getChatId(),
                            "Кожаный ублюдок! ты велел напомнить: " + reminderText,
                            LocalDateTime.now().plusMinutes(delay)
                    );
                    Reminder reminder2 = new Reminder(
                            userDetails.getChatId(),
                            reminderText + " НЕ ЗАБУДЬ БЛЕТЬ!",
                            LocalDateTime.now().plusMinutes(delay+15)
                    );
                    try {
                        JedisProvider.addToRemindersMap(reminder1);
                        JedisProvider.addToRemindersMap(reminder2);
                    } catch (JsonProcessingException e) {
                        System.out.println("Jedis died " + e.toString());
                    }

                    return "Напоминалка "+ reminderText + " установлена";
                }
            }
            reply = "Неправильный формат ввода!! Сперва МИНУТЫ ПОТОМ ТЕКСТ!";
        }
        return reply;
    }

}
