package cybot.kafka;

import cybot.PropsLoader;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class MainProducer {
    private static final String topic = PropsLoader.getInstance().getCLOUDKARAFKA_USERNAME() + "-default"; ;
    private static final Properties props = KafkaProperties.getKafkaProperties();
    private static KafkaProducer<String, String> producer = getProducer();

    public static void produce(String key, String message) {
        try {
            KafkaProducer<String, String> producer = getProducer();
            //Create ProducerRecord
            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(topic, key, message);
            //Do something
            producer.send(producerRecord);
            //Send
            producer.flush();
        } catch (Exception ex) {
            //Close
            System.out.println(ex.toString());
            producer.close();
            producer = null;
        }
    }

    public static KafkaProducer<String, String> getProducer() {
        if (producer == null) {
            System.out.println("Generating new producer");
            KafkaProducer<String, String> producerToGo = new KafkaProducer<String, String>(props);
            return producerToGo;
        } else {
            return producer;
        }
    }

}