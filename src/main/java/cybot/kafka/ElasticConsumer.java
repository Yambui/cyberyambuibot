package cybot.kafka;

import cybot.PropsLoader;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ElasticConsumer implements Runnable {
    private static final String topic = PropsLoader.getInstance().getCLOUDKARAFKA_USERNAME() + "-default"; ;
    private static final Properties props = KafkaProperties.getKafkaProperties();
    private static KafkaConsumer<String, String> consumer = getConsumer();
    private static RestHighLevelClient client = createClient();

    public static RestHighLevelClient createClient() {
        if (client == null) {
            System.out.println("Creating rest client");
            String hostname = PropsLoader.getInstance().getHostname(); // localhost or bonsai url
            String username = PropsLoader.getInstance().getUsername(); // needed only for bonsai
            String password = PropsLoader.getInstance().getPassword(); // needed only for bonsai

            // credentials provider help supply username and password
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,
                    new UsernamePasswordCredentials(username, password));

            RestClientBuilder builder = RestClient.builder(
                    new HttpHost(hostname, 443, "https"))
                    .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                        @Override
                        public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                            return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                        }
                    });

            RestHighLevelClient clientToGo = new RestHighLevelClient(builder);
            return clientToGo;
        } else {
            return client;
        }
    }

    public static void sendToElastic(String message){

        RestHighLevelClient client = createClient();

        IndexRequest indexRequest = new IndexRequest("cyberyambui").source(message, XContentType.JSON);

        try {
            IndexResponse response = client.index(indexRequest, RequestOptions.DEFAULT);
            System.out.println("Sent to elastic " + response.getId());
        } catch (Exception e) {
            System.out.println("Elastic exception - "+ e.toString());;
        } finally {
            indexRequest = null;
        }

    }

    private static KafkaConsumer<String, String> getConsumer() {
        if (consumer == null) {
            System.out.println("Creating new consumer");
            KafkaConsumer<String, String> consumerToGo = new KafkaConsumer<>(props);
            consumerToGo.subscribe(Arrays.asList(topic));
            return consumerToGo;
        } else {
            return consumer;
        }
    }

    @Override
    public void run() {
        try {
            final long end = System.nanoTime() + 6 * 1000 * 1000 * 1000L;
            int loop = 1;
            do {
                ConsumerRecords<String, String> records = getConsumer().poll(Duration.ofMillis(2000));
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println("Got record from user - " + record.key());
                    sendToElastic(record.value());
                }
                loop++;
            } while (System.nanoTime() < end);
            System.out.println("Pool was done " + loop + " times");
        } catch (Exception ex) {
            System.out.println("Exception ex in kafka listener" + ex.toString());
            consumer.close();
            consumer = null;
        }
    }

}
