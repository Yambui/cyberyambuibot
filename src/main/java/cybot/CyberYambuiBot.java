package cybot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cybot.kafka.MainProducer;
import cybot.redis.JedisProvider;
import cybot.reminder.Reminder;
import cybot.reminder.ReminderScheduler;
import cybot.userdetails.UserDetails;
import org.glassfish.grizzly.utils.Pair;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static cybot.util.Contstants.CURRENCY;
import static cybot.util.Contstants.CURRENCY2;
import static cybot.util.Contstants.GAME;
import static cybot.util.Contstants.GAME2;
import static cybot.util.Contstants.GAME3;
import static cybot.util.Contstants.GAME4;
import static cybot.util.Contstants.HELP;
import static cybot.util.Contstants.HORO;
import static cybot.util.Contstants.HORO2;
import static cybot.util.Contstants.SMART;
import static cybot.util.Contstants.SMART2;
import static cybot.util.Contstants.START;
import static cybot.util.Contstants.WEATHER;
import static cybot.util.Contstants.WEATHER2;

public class CyberYambuiBot extends TelegramLongPollingBot implements Runnable{

    private final PropsLoader propsLoader = PropsLoader.getInstance();
    private final TextResponder textResponder = new TextResponder();
    private final ObjectMapper mapper = new ObjectMapper();
    private final Map<Integer, UserDetails> userDetailsMap = new HashMap<>();

    @Override
    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message_text = update.getMessage().getText();
            produceMessageToKafka(update);
            UserDetails userDetails = getUserDetails(update);
            messageCheckerStringResponder(message_text, userDetails);
        }
    }

    private UserDetails getUserDetails(Update update) {
        Integer userId = update.getMessage().getFrom().getId();
        if (userDetailsMap.get(userId) == null ){
            userDetailsMap.put(userId, new UserDetails(update.getMessage().getChatId()));
        }
        return userDetailsMap.get(userId);
    }

    private void produceMessageToKafka(Update update) {
        String username = Optional.ofNullable(update.getMessage().getFrom().getUserName()).orElse("no_username");
        String key = update.getMessage().getFrom().getId()+"_"+username;
        String value = "Unable to parse";
        try {
            value = mapper.writeValueAsString(update.getMessage());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        MainProducer.produce(key, value);
    }

    private void messageCheckerStringResponder(String message_text, UserDetails userDetails) {
        String response = "";
        SendMessage messg = new SendMessage().setChatId(userDetails.getChatId());
        if (userDetails.getRpsGameStarted() || userDetails.getTicGameStarted() || userDetails.getHoroscopeStarted()) {
            processStartedEvent(messg, message_text, userDetails);
            return;
        }

        switch (message_text) {
            case SMART:
            case SMART2: {
                response = textResponder.getRandomIdiom();
            }
            break;
            case CURRENCY:
            case CURRENCY2: {
                response = textResponder.getCurrencyRate();
            }
            break;
            case WEATHER:
            case WEATHER2: {
                response = textResponder.getWeatherForecast();
            }
            break;
            case GAME:
            case GAME2: {
                messg = userDetails.getRpsGame().rpsGameStart(messg);
                userDetails.setRpsGameStarted(true);
            }
            break;
            case GAME3:
            case GAME4: {
                messg = userDetails.getTicTacToeGame().ticGameStart(messg);
                userDetails.setTicGameStarted(true);
            }
            break;
            case HORO:
            case HORO2: {
                messg = userDetails.getHoroscopeResponder().horoscopeStart(messg);
                userDetails.setHoroscopeStarted(true);
            }
            break;
            case HELP:
            case START: {
                response = textResponder.getStartMessage();
            }
            break;
            default: {
                response = ReminderScheduler.tryToScheduleReminder(message_text, userDetails);
            }
        }
        if (messg.getText() == null) {
            messg.setText(response);
        }
        sendMessage(messg);
    }
    
    private void sendMessage(SendMessage message){
        try {
            execute(message); // Sending our message object to user
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        
    }

    private void processStartedEvent(SendMessage messg, String message_text, UserDetails userDetails) {
        if (userDetails.getRpsGameStarted()) {
            messg.setText(message_text);
            messg = userDetails.getRpsGame().doGame(messg);
            userDetails.setRpsGameStarted(false);
            sendMessage(messg);
            return;
        }
        if (userDetails.getTicGameStarted()) {
            messg.setText(message_text);
            Pair results = userDetails.getTicTacToeGame().humanInput(messg);
            messg = (SendMessage) results.getSecond();
            if ((boolean)results.getFirst()) {
                userDetails.setTicGameStarted(false);
            }
            sendMessage(messg);
            return;
        }
        if (userDetails.getHoroscopeStarted()) {
            messg.setText(message_text);
            Pair results = userDetails.getHoroscopeResponder().processResponseFromUser(messg);
            messg = (SendMessage) results.getSecond();
            if ((boolean)results.getFirst()) {
                userDetails.setHoroscopeStarted(false);
            }
            sendMessage(messg);
            return;
        }
    }

    @Override
    public String getBotUsername() {
        // Return bot username
        // If bot username is @MyAmazingBot, it must return 'MyAmazingBot'
        return propsLoader.getBotname();
    }

    @Override
    public String getBotToken() {
        // Return bot token from BotFather
        return propsLoader.getBottoken();
    }

    @Override
    public void run() {
        System.out.println("Reminding job started");
        for (String key: JedisProvider.getAllKeys()) {
            System.out.println(key);
            try {
                if (LocalDateTime.parse(key).isBefore(LocalDateTime.now())) {
                    Reminder reminder = JedisProvider.getReminder(key);
                    SendMessage reminderMessage = new SendMessage().setChatId(reminder.getChatId());
                    reminderMessage.setText(reminder.getReminderText());
                    sendMessage(reminderMessage);
                    JedisProvider.removeReminder(key);
                }
            } catch (Exception e) {
                System.out.println("Reminder job exception " + e.toString());
                continue;
            }
        }
        System.out.println("Reminding job ended");
    }

}
