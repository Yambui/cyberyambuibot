package cybot;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.Properties;

public class PropsLoader {

    private static final PropsLoader propsloader = new PropsLoader();
    private String botname;
    private String bottoken;
    private String weathertoken;
    private int ukrIdiomsNumberOfLines;
    private String CLOUDKARAFKA_BROKERS;
    private String CLOUDKARAFKA_USERNAME;
    private String CLOUDKARAFKA_PASSWORD;
    private String hostname;
    private String username;
    private String password;
    private String REDIS_HOST;
    private String REDIS_PASSWORD;

    public PropsLoader() {
        InputStream inputStream = null;
        try {

            botname = System.getenv("botname");
            bottoken = System.getenv("bottoken");
            weathertoken = System.getenv("weathertoken");
            CLOUDKARAFKA_BROKERS = System.getenv("CLOUDKARAFKA_BROKERS");
            CLOUDKARAFKA_USERNAME = System.getenv("CLOUDKARAFKA_USERNAME");
            CLOUDKARAFKA_PASSWORD = System.getenv("CLOUDKARAFKA_PASSWORD");
            hostname = System.getenv("elastic");
            username = System.getenv("accountacc");
            password = System.getenv("accsecret");
            REDIS_HOST = System.getenv("REDIS_HOST");
            REDIS_PASSWORD = System.getenv("REDIS_PASSWORD");

            File file = File.createTempFile("tempfile", ".tmp");
            InputStream inputStream1 = getClass().getClassLoader().getResourceAsStream("ukridioms.txt");
            FileUtils.copyInputStreamToFile(inputStream1, file);
            ukrIdiomsNumberOfLines = countLines(file);
            System.out.println(ukrIdiomsNumberOfLines);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    static int countLines(File file) throws IOException {
        int count = 0;
        boolean empty = true;
        FileInputStream fis = null;
        InputStream is = null;
        try {
            fis = new FileInputStream(file);
            is = new BufferedInputStream(fis);
            byte[] c = new byte[1024];
            int readChars = 0;
            boolean isLine = false;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        isLine = false;
                        ++count;
                    } else if (!isLine && c[i] != '\n' && c[i] != '\r') {   //Case to handle line count where no New Line character present at EOF
                        isLine = true;
                    }
                }
            }
            if (isLine) {
                ++count;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return (count == 0 && !empty) ? 1 : count;
    }

    public String getBotname() {
        return botname;
    }

    public String getBottoken() {
        return bottoken;
    }

    public String getWeathertoken() {
        return weathertoken;
    }

    public String getCLOUDKARAFKA_BROKERS() {
        return CLOUDKARAFKA_BROKERS;
    }

    public String getCLOUDKARAFKA_USERNAME() {
        return CLOUDKARAFKA_USERNAME;
    }

    public String getCLOUDKARAFKA_PASSWORD() {
        return CLOUDKARAFKA_PASSWORD;
    }

    public String getHostname() {
        return hostname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getREDIS_HOST() {
        return REDIS_HOST;
    }

    public String getREDIS_PASSWORD() {
        return REDIS_PASSWORD;
    }


    public int getUkrIdiomsNumberOfLines() {
        return ukrIdiomsNumberOfLines;
    }

    public static PropsLoader getInstance() {
        return propsloader;
    }
}
